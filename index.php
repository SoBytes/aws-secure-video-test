<!DOCTYPE html>
<html lang="en">
   <head>
        <title>BitStreamin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="./dist/bitstreamin.min.css?ver=" rel="stylesheet" type="text/css" /> 
   </head>
<body>

    <video id="bitstreamin" controls autoplay muted="muted" class="videoCentered"></video>
    
      
    <script src="./dist/bitstreamin.js?ver=" type="text/javascript"></script> 
    <script type='text/javascript'>

        BitStreamin("bitstreamin").video({
            src: 'https://dwpurpmwfzvap.cloudfront.net/maiden-trailer-1-h1080p/master.m3u8'
        }, function(player) {

            console.log('player',player);

            setTimeout(function(){

                player.pause();

            },2000);
        }); 

   </script>         
   </body>
</html>
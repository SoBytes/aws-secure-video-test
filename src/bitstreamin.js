function BitStreamin(div) {

    // Create global      
    var BitStreamin = {}; 

    // Check for video element
    if(!(div instanceof Element)){
        video = document.getElementById(div);  
    }

    // Check that the user has added the container to the document or clean up
    if (div === null) {
        console.log("ERROR: Please make sure your html div has a unique id element");
        return;
    }

    /**
     * Build the main player 
     * @public 
     */
    BitStreamin.video = function(options, callback) {

        //console.log('options',options); 
        //console.log('div',div); 

        var that = this;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'ajax.php');
        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK){ 
                    console.log(xhr.responseText); // 'This is the returned text.'
                    document.cookie="Authorization=" + xhr.responseText;
                    that.loadPlayer(options,callback); 

                } else {
                    console.log('Error: ' + xhr.status); // An error occurred during the request.
                }
            }
        };
        xhr.send(null);             
     
    };

    BitStreamin.loadPlayer = function(options, callback) {

        if (Hls.isSupported()) {
            
            var config = {
                xhrSetup: function(xhr, url) {
                    //xhr.withCredentials = false; // do send cookie
                    //xhr.setRequestHeader("Authorization", '792124a93ff93daaf8d68e1a939470527c46036ee128826df53afb6e980a34b4');
                }
            };

            var hls = new Hls(config);
            // bind them together
            hls.attachMedia(video);
            hls.on(Hls.Events.MEDIA_ATTACHED, function () {
                console.log("video and hls.js are now bound together !");
                hls.loadSource(options.src);
                hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
                    console.log("manifest loaded, found " + data.levels.length + " quality level");
                });
            });

        }else if (video.canPlayType('application/vnd.apple.mpegurl')) {

            video.src = options.src;
            video.addEventListener('loadedmetadata',function() {
                video.play();
            });

        }else{

            /*var url = "https://dash.akamaized.net/envivio/EnvivioDash3/manifest.mpd";
            var player = dashjs.MediaPlayer().create();

            var licenseToken;
            player.setProtectionData({
                "com.widevine.alpha": {
                    "serverURL": "https://widevine-dash.ezdrm.com/widevine-php/widevine-foreignkey.php?pX=CA6D8D"
                }
            });*/

            player.initialize(video, options.src, true);

        }

        video.addEventListener('loadedmetadata',function() {
            console.log('loaded');
            this.play();
            callback(this);
        });

    };

    return BitStreamin;

}